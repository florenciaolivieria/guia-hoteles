js: masonry.pkgd.js

var exampleModal = document.getElementById('contacto')
exampleModal.addEventListener('show.bs.modal', function (e) {
    console.log('el modal se esta mostrando');
    document.getElementById('contactoBtn').classList.replace('btn-outline-primary', 'disabled');
})

exampleModal.addEventListener('hide.bs.modal', function (e) {
    console.log('el modal se esta ocultando');
    document.getElementById('contactoBtn').classList.replace('disabled', 'btn-outline-primary');
})
exampleModal.addEventListener('hidden.bs.modal', function (e) {
    console.log('el modal se oculto');
})

exampleModal.addEventListener('shown.bs.modal', function (e) {
    console.log('el modal se mostro');
   // document.getElementById('contactoBtn').classList.replace('btn-outline-primary', 'disabled');    
})